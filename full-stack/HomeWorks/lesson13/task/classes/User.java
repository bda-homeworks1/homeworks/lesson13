package HomeWorks.lesson13.task.classes;

public class User {
    String username;
    int birthdayYear;
    String password;

    int age;

    public User(String username, int birthdayYear, String password, int age) {
        this.username = username;
        this.birthdayYear = birthdayYear;
        this.password = password;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", birthdayYear=" + birthdayYear +
                ", password='" + password + '\'' +
                ", age=" + age +
                '}';
    }
}
