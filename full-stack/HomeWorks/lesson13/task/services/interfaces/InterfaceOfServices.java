package HomeWorks.lesson13.task.services.interfaces;

import HomeWorks.lesson13.task.Exception.MyException;

public interface InterfaceOfServices {
    void showMenu() throws MyException;
    void register() throws MyException;
    void showPeople() throws MyException;
}
